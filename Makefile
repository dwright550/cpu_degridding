# *****************************************************
# Variables to control Makefile operation
 
CC = g++
CFLAGS = -Wall -g
 
# ****************************************************
# Targets needed to bring the executable up to date
 
SKA_cpu_degrid_test: SKA_cpu_degrid_test.o
	$(CC) $(CFLAGS) -o SKA_cpu_degrid_test.exe SKA_cpu_degrid_test.o
 

 
SKA_cpu_degrid_test.o: SKA_cpu_degrid_test.cpp
	$(CC) $(CFLAGS) -c SKA_cpu_degrid_test.cpp
 
clean:	
	rm -f *.o *.~ SKA_cpu_degrid_test.exe
